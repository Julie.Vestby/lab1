package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.lang.model.util.ElementScanner14;

import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        while (true){

            System.out.println ("Let's play round " + roundCounter);

            String humanChoice = humanChoice();
            String computerChoice = computerChoice ();
        
            if(humanChoice.equals(computerChoice)){
                System.out.println ("Human chose " + humanChoice + ", computer chose " + computerChoice + ". It's a tie!");
            }

            else if(humanChoice.equals ("rock") && computerChoice.equals ("scissors")){
                System.out.println ("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
                humanScore ++;
            } else if (humanChoice.equals ("rock") && computerChoice.equals ("paper")) {
                System.out.println ("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                computerScore ++;
            }

            else if(humanChoice.equals ("paper") && computerChoice.equals ("rock")){
                System.out.println ("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
                humanScore ++;
            } else if (humanChoice.equals ("paper") && computerChoice.equals ("scissors")) {
                System.out.println ("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                computerScore ++;
            }

            else if(humanChoice.equals ("scissors") && computerChoice.equals ("paper")){
                System.out.println ("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
                humanScore ++;
            } else if (humanChoice.equals ("scissors") && computerChoice.equals ("rock")) {
                System.out.println ("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                computerScore ++;
            }

            System.out.println ("Score: human " + humanScore + ", computer " + computerScore + " ");
            String cont = readInput ("Do you wish to continue playing? (y/n)?");
            if (!cont.equals ("y")){
                System.out.println ("Bye bye :)");
                break;
            }
            roundCounter ++;
        }
    }


    public String humanChoice() {

        while(true) {
            
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");

            if(!rpsChoices.contains(humanChoice)) {
                System.out.println("I don't understand " + humanChoice + ". Could you try again?");
            }
            else {
                return humanChoice;
            }}


        }
    
    public String computerChoice() {

        Random rand = new Random();
        int index  = rand.nextInt(rpsChoices.size());

        return rpsChoices.get(index);
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
